mod init;
use devx_cmd::run;
use init::{init, GlobalOpts, StartOpts, Subcommand};
use log::{info, warn};
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
	let global_opts = init();

	sudo::escalate_if_needed()?;

	match &global_opts.subcommand {
		Subcommand::Stop => {
			stop(&global_opts);
			Ok(())
		}
		Subcommand::Start(cmd_opts) => {
			start(&global_opts, cmd_opts);
			Ok(())
		}
	}
}

fn stop(global_opts: &GlobalOpts) {
	let GlobalOpts { interface, .. } = global_opts;

	info!("bringing down the interface...");
	match run!("ip", "link", "set", &interface, "down") {
		Ok(_) => info!("done."),
		Err(_) => warn!("failed to bring down the interface. it probably doesn't yet exist."),
	};

	info!("killing `slcand...`");
	match run!("killall", interface) {
		Ok(_) => info!("done."),
		Err(_) => warn!("failed to kill `slcand`. assuming it was not running."),
	};
}

fn start(global_opts: &GlobalOpts, cmd_opts: &StartOpts) {
	let GlobalOpts { interface, .. } = global_opts;
	let StartOpts { device } = cmd_opts;

	info!("stopping the bridge in case it is already running.");
	stop(global_opts);

	info!("starting `slcand`...");
	run!("slcand", "-o", "-c", "-s5", "-S3000000", device, interface)
		.expect("failed to start `slcand`.");
	info!("done.");

	info!("bringing up the interface...");
	run!("ip", "link", "set", interface, "up").expect("failed to bring up the interface.");
	info!("done.");
}
